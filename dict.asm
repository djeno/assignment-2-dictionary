%include "lib.inc"
%define OFFSET 8

section .text
global find_word

; find_word принимает на вход два аргумента 
; 1) нуль-терминированную строку (rdi) 
; 2) указатель на начало словаря (rsi)
find_word:
	push r12
	push r13
	mov r12, rdi 
	mov r13, rsi
	.loop:
		test r13, r13 
		jz .fail
		mov rdi, r12
		mov rsi, r13
		add rsi, OFFSET
		call string_equals
		test rax, rax
		jnz .success
		mov r13, [r13]
		jmp .loop
	.success:
		mov rax, r13
		pop r13
		pop r12
		ret
	.fail:
		xor rax, rax
		pop r13
		pop r12
		ret
