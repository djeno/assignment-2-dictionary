COMPILER = nasm -g 
COMPILER_ATTR = -felf64 -o
LINKER = ld -o

lib.o: lib.asm
	$(COMPILER) lib.asm $(COMPILER_ATTR) lib.o
	
dict.o: dict.asm lib.o
	$(COMPILER) dict.asm $(COMPILER_ATTR) dict.o
	
main.o: main.asm dict.o lib.o 
	$(COMPILER) main.asm $(COMPILER_ATTR) main.o
	
program: main.o dict.o lib.o 
	$(LINKER) program main.o dict.o lib.o 

.PHONY: all clean test

all: program

clean:
	rm *.o program

test: 
	python3 test.py	
