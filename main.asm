%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define PRINT_SYSCALL 1
%define STDERR 2

%define BUF_SIZE 256

section .rodata
    key_key_not_found_mes: db "Key not found", 0
    incorrect_input_mes: db "Incorrect input", 0

section .data
    buffer: times BUF_SIZE db 0

section .text
    global _start:

_start:
    ; читаем ввод
	mov rdi, buffer
	mov rsi, BUF_SIZE
	call read_word
	test rax, rax 
	jz .incorrect_input
	
    ; проверяем на совпадение
	push rdx	
	mov rdi, buffer
	mov rsi, KEY
	call find_word
	pop rdx	
	test rax, rax
	jz .key_not_found
	lea rdi, [rax + 8 + rdx + 1] 
	call print_string
	jmp .end
	.key_not_found:
		mov rdi, key_key_not_found_mes
		jmp .print_error
	.incorrect_input:
		mov rdi, incorrect_input_mes
    .print_error:
        push rdi
        call string_length
        mov rdx, rax
        pop rsi
        mov rax, PRINT_SYSCALL
        mov rdi, STDERR
        syscall
	.end:
		call print_newline
		call exit



