import subprocess

input_string = ["", "first", "third", "random", "f"*300]
output_string = ["", "ITMO", "way",  "", ""]
error_string = ["Key not found", "", "", "Key not found", "Incorrect input"]

for i in range(len(input_string)):
    task = subprocess.Popen(["./program"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = task.communicate(input=input_string[i].encode())
    program_output = stdout.decode().strip()
    program_error = stderr.decode().strip()
    if (program_output == output_string[i] and program_error == error_string[i]):
        print('Test '+str(i+1)+': passed\n\tstdin: "'+input_string[i]+'"\n\tstrout: "'+program_output+'"\n\tstrerr: "'+program_error+'"')
    else:
        print("Test "+str(i+1)+': failed\n\tstdin: "'+input_string[i]+'"\n\tstrout: "'+program_output+'"\n\tstrerr: "'+program_error+'"')
